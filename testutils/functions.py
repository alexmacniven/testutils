"""testutils.api.functions

Implements the functions module
"""
import sqlite3
import os
import json


class MockResponse:
    """A MockResponse object"""

    def __init__(self, data, status_code):
        self.text = data
        self.status_code = status_code

    def json(self):
        return json.loads(self.text)


def remove_readonly(func, path, excinfo):
    """Removes 'readonly' permission on the path"""
    import stat
    os.chmod(path, stat.S_IWRITE)
    func(path)


def build_routes_table(path, table):
    """Creates a routes table in the Routes.db at the path

    Args:
        path(str): Path to Routes.db
        table(str): Table name
    """
    conn = sqlite3.connect(os.path.join(path, 'Routes.db'))
    query = '''CREATE TABLE {} (
        Depart TEXT,
        Arrive TEXT,
        Next INTEGER DEFAULT 0,
        Working NUMERIC DEFAULT 0);'''.format(table)
    conn.execute(query)
    conn.commit()
    conn.close()


def add_route_record(path, table, depart, arrive, next, working):
    """Adds a new record to the table in the Routes.db at the path

    Args:
        path(str): Path to Routes.db
        table(str): Table name
        depart(str): Departure airport
        arrive(str): Arrival airport
        next(int): Next process time
        working(bool): Current working status
    """
    conn = sqlite3.connect(os.path.join(path, 'Routes.db'))
    query = '''INSERT INTO {}
        VALUES (?,?,?,?);'''.format(table)
    conn.execute(query, (depart, arrive, next, int(working),))
    conn.commit()
    conn.close()


def get_route_record(path, table, depart, arrive):
    """Gets the record from the table which matches depart and arrive

    Args:
        path(str): Path to Routes.db
        table(str): Table name
        depart(str): Depart airport
        arrive(str): Arrive airport
    """
    conn = sqlite3.connect(os.path.join(path, 'Routes.db'))
    query = '''SELECT *
        FROM {}
        WHERE Depart=?
        AND Arrive=?;'''.format(table)
    cursor = conn.execute(query, (depart, arrive,))
    rec = cursor.fetchone()
    conn.close()
    return rec
