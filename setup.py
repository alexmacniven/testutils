# This is a template setup.py file. You should only need to change the
# Package meta-data section.

# Note: To use the 'upload' functionality of this file, you must:
#   $ pip install twine

import codecs
import os
import sys

from setuptools import find_packages, setup, Command
from shutil import rmtree

# Package meta-data.
NAME = "testutils"
DESCRIPTION = "Commonly used functionality for unit testing"
URL = "https://bitbucket.org/alexmacniven/testutils"
EMAIL = 'apmacniven@outlook.com'
AUTHOR = 'Alex Macniven'

# What packages are required for this module to be executed?
REQUIRED = [
]

# The rest you shouldn't have to touch too much :)
# ------------------------------------------------
# Except, perhaps the License and Trove Classifiers!

here = os.path.abspath(os.path.dirname(__file__))

# Import the README and use it as the long-description.
# Note: this will only work if 'README.rst' is present in your MANFEST.in file!
with codecs.open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = '\n' + f.read()

# Load the package's __version__.py module as a dictionary.
about = {}
with open(os.path.join(here, NAME, '__version__.py')) as f:
    exec(f.read(), about)


# Where the magic happens:
setup(
    name=NAME,
    version=about['__version__'],
    description=DESCRIPTION,
    long_description=long_description,
    author=AUTHOR,
    author_email=EMAIL,
    url=URL,
    packages=find_packages(exclude=('tests',)),
    install_requires=REQUIRED,
    include_package_data=True,
    license='ISC',
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'License :: OSI Approved :: ISC License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
    ]
)
