# testutils

Commonly used functionality for unit testing

`class MockResponse`
--------------------

A class to mock a response object as used by `requests.get`

`remove_readonly`
-----------------

When using `shutil.rmtree` to remove directories created with `tempfile.mkdtemp`,
errors can often be raised based on permissions on the path.

`remove_readonly` will change the 'write' permissions on the path, eg;

```
shutil.rmtree(path, onerror=testutils.remove_readonly)
```

`build_routes_table`
--------------------

Connects to a 'Routes.db' at the given `path` and creates a routes table called
`table`

```
testutils.build_routes_table(path, table)
```

`add_route_record`
------------------

Connects to a 'Routes.db' at the given `path` and adds a record to the `table`

```
testutils.add_route_record(path, table, depart, arrive, next, working)
```

`get_route_record`
------------------

Connects to a 'Routes.db' at the given `path` and returns the first record from
`table` mathing the `depart` and `arrive` values

```
testutils.get_route_record(path, table, depart, arrive)
```
